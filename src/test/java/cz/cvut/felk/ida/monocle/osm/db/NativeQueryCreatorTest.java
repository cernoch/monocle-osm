/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.db;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pavel Vňuk <vnukpave@fel.cvut.cz>
 */
public class NativeQueryCreatorTest {
    
    public NativeQueryCreatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of configure method, of class NativeQueryCreator.
     */
    @Test
    public void testConfigure() {
        System.out.println("configure");
        NativeQueryCreator instance = new NativeQueryCreator();
        instance.configure();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of generateQuery method, of class NativeQueryCreator.
     */
    @Test
    public void testGenerateQuery() {
        System.out.println("generateQuery");
        NativeQueryCreator instance = new NativeQueryCreator();
        StringBuilder expResult = new StringBuilder();
        expResult.append("select * from planet_osm_rels where ((tags[3]='kct_barva' AND tags[4]='ski') OR (tags[5]='kct_barva' AND tags[6]='ski') OR (tags[7]='kct_barva' AND tags[8]='ski') OR (tags[9]='kct_barva' AND tags[10]='ski')" +
        " OR (tags[3]='kct_barva' AND tags[4]='bicycle') OR (tags[5]='kct_barva' AND tags[6]='bicycle') OR (tags[7]='kct_barva' AND tags[8]='bicycle') OR (tags[9]='kct_barva' AND tags[10]='bicycle')" +
        " OR (tags[3]='kct_barva' AND tags[4]='major') OR (tags[5]='kct_barva' AND tags[6]='major') OR (tags[7]='kct_barva' AND tags[8]='major') OR (tags[9]='kct_barva' AND tags[10]='major')" +
        " OR (tags[3]='kct_barva' AND tags[4]='local') OR (tags[5]='kct_barva' AND tags[6]='local') OR (tags[7]='kct_barva' AND tags[8]='local') OR (tags[9]='kct_barva' AND tags[10]='local')" +
        " OR (tags[3]='kct_barva' AND tags[4]='learning') OR (tags[5]='kct_barva' AND tags[6]='learning') OR (tags[7]='kct_barva' AND tags[8]='learning') OR (tags[9]='kct_barva' AND tags[10]='learning')" +
        ");");
        instance.configure();
        StringBuilder result = instance.generateOSMQuery();
        assertEquals(expResult.length(), result.length());
        
    }
    
}
