/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

/**
 *
 * @author Pavel Vňuk
 */
@Entity
@Table(name = "way_map")
@Struct(name = "REL_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WayMap.findAll", query = "SELECT p FROM WayMap p"),
    @NamedQuery(name = "WayMap.findById", query = "SELECT p FROM WayMap p WHERE p.id = :id")})
public class WayMap implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "new_ways")
    @Array(databaseType = "bigint[]")
    private List<Long> newWays;

    public WayMap() {
    }

    public WayMap(Long id){
        this.id = id;
    }
    
    public WayMap(Long id, List<Long> newWays){
        this.id = id;
        this.newWays = newWays;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getNewWays() {
        return newWays;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WayMap)) {
            return false;
        }
        WayMap other = (WayMap) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.felk.ida.monocle.osm.db.WayMap[ id=" + id + " ]";
    }
    
}
