/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.configs;

/**
 *
 * @author Pavel Vňuk
 */
public class DBConfig {
    
    private static DBConfig instance;
    
    private String pathToXMLFile;
    private String pathToRelIds;

    private DBConfig(){
        this.pathToXMLFile = "xml_output";
        this.pathToRelIds = "rel_ids";
    }
    
    public static DBConfig getInstance(){
        if(instance == null)instance = new DBConfig();
        return instance;
    }

    public String getPathToRelIds() {
        return pathToRelIds;
    }

    public void setPathToRelIds(String pathToRelIds) {
        this.pathToRelIds = pathToRelIds;
    }
           
    public String getPathToXMLFile() {
        return pathToXMLFile;
    }

    public void setPathToXMLFile(String pathToXMLFile) {
        this.pathToXMLFile = pathToXMLFile;
    }
    
    
}
