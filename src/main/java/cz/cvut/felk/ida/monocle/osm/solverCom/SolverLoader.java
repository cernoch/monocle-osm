/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.solverCom;

import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.model.UnderlyingGraph;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class directly responsible for creating MLCMP problem object required by solver.
 * Works as substitute for DataLoader present in MLCMP Solver packages, which works
 * only with XML files. This alternative operates with database based input {@code DBHandler},
 * either for whole fresh solution or partial diff-update.
 * @author Radomír Černoch
 * @author Pavel Vňuk
 */
public class SolverLoader {
    
    private MLCMP problem;
    
    private PrintWriter out;
    
    private final DataStore ds;
    
    private final Map<Long,Station> stations;
    private final Map<Station,Long> stationNodeMap;
    
    private Map<Long, Station> replaceStationMap;
           
    private final Map<Long,List<Long>> relWayMap;
    private final Map<Long,List<Long>> wayNodeMap;
    
    private final Map<String,List<Long>> lineRelMap;
    
    private boolean buildComplete;
    
    private final int mode;
    
    /**
     *
     * @param lineNames
     * @param mode
     */
    public SolverLoader(Set<String> lineNames, int mode){
        this.ds = DataStore.getInstance();
        
        this.mode = mode;
        
        stations = ds.getStations();
        stationNodeMap = ds.getStationNodeMap();
        replaceStationMap = ds.getReplaceStationMap();
        relWayMap = ds.getRelWayMap();
        wayNodeMap = ds.getWayNodeMap();
        lineRelMap = ds.getLineRelMap();
        
        for (String lineName : lineNames) {
            lineRelMap.put(lineName, new ArrayList<Long>());
        }
        
        buildComplete = false;
    }

    private void phase1(){
        for (PlanetOsmNodes node : ds.getNodes().values()) {
            Station s = new Station(node.getLon(), node.getLat());
            stations.put(node.getId(),s);
            stationNodeMap.put(s, node.getId());
        }
    }
    
    private void phase2(){
        for (PlanetOsmWays way : ds.getWays().values()) {
            wayNodeMap.put(way.getId(), way.getNodes());
        }
    }
    
    private void phase3(){
        HashSet<Long> problematicWays = ds.getProblematicWays();
        ArrayList<Long> ways;   
        for (PlanetOsmRels rel : ds.getRelations().values()) {
            lineRelMap.get(rel.getLabel()).add(rel.getId());
            if(mode == 0){
                relWayMap.put(rel.getId(), rel.getWays());
            }else{
                ways = new ArrayList<>();
                for (Long way : rel.getWays()) {
                    if(problematicWays.contains(way))ways.add(way);
                }
                relWayMap.put(rel.getId(), ways);
            }
            // add only ways needed - all in FR 
        }
    }
    
    private void constructGraph(){
        final UnderlyingGraph graph = new UnderlyingGraph();
        Map<Long, Station> duplicateStations = new HashMap<>();
        // at first add all stations to the graph
        for (Map.Entry<Long, Station> e : stations.entrySet()){
                Station s = e.getValue();
                // Czech trails are a little bit buggy - there are nodes with same coordinate
                if (!graph.contains(s))
                        graph.addVertex(s);
                else
                        duplicateStations.put(e.getKey(), graph.getStoredStation(s));
        }
        
        final LinkedList<Line> lines = new LinkedList<>();
        final ArrayList<Track> lineTracks = new ArrayList<>(1024);

        Set<Long> waysMap;
        
        for (String lineName : lineRelMap.keySet()){
            
            waysMap = new HashSet<>();
            for (Long id : lineRelMap.get(lineName)) {
                waysMap.addAll(relWayMap.get(id));
            }
            
            for (Long wayId : waysMap){
                //skip ways not in graph component --- DIFF mode only
                if(!ds.getWays().containsKey(wayId))continue;
                // get the nodes list
                List<Long> way = wayNodeMap.get(wayId);
                // bad thig - corrupted XML that contains refference to way that is not specified
                if (way == null){
                        if (out != null)
                                out.println("Found refference to way that is not specified in the file : " + wayId);
                        continue;
                }

                final int size = way.size();

                if (size <= 1){
                        if (out != null)
                                out.println("Illegal way - no or one node.");
                        continue;
                }

                // now create tracks and paste them into line
                final Long first = way.get(0);
                Station u = duplicateStations.containsKey(first) ? duplicateStations.get(first) : stations.get(first);
                for (int i = 1; i < size; i++){
                        final Long id = way.get(i);
                        final Station v = duplicateStations.containsKey(id) ? duplicateStations.get(id) : stations.get(id);
                        // we don't want tracks that have both endpoints same
                        if (u.equals(v))
                                continue;
                        Track t = new Track(u, v);
                        ds.getTrackWayMap().put(t, new ArrayList<Long>());
                        ds.getTrackWayMap().get(t).add(id);
                        ds.getWayTrackMap().put(id, new ArrayList<Track>());
                        ds.getWayTrackMap().get(id).add(t);
                        // don't try to add to graph track if it already exists
                        if (graph.contains(t))
                                t = graph.getStoredTrack(t);
                        else
                                graph.addEdge(t);

                        // and add track to current line
                        lineTracks.add(t);
                        u = v;
                }
            }

            // remember the just created line
            lines.add(new Line(lineName,lineTracks));
            lineTracks.clear();
        }

        problem = new MLCMP(graph, lines);
        replaceStationMap = duplicateStations;
        buildComplete = true;
    }
    
    /**
     *
     */
    public void loadSolver(){
        phase1();
        phase2();
        phase3();
        constructGraph();
    }
    
    /**
     * Returns the loaded problem.
     *
     * @return The problem.
     */
    public MLCMP getProblem()
    {
            if (!buildComplete)
                    throw new IllegalStateException("Graph must be constructed before.");
            return problem;
    }
}
