/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.cvut.felk.ida.monocle.osm.db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

/**
 *
 * @author Pavel Vňuk
 */
@Entity
@Table(name = "planet_osm_nodes")
@Struct(name = "REL_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanetOsmNodes.findAll", query = "SELECT p FROM PlanetOsmNodes p"),
    @NamedQuery(name = "PlanetOsmNodes.findById", query = "SELECT p FROM PlanetOsmNodes p WHERE p.id = :id"),
    @NamedQuery(name = "PlanetOsmNodes.findByIds", query = "SELECT p FROM PlanetOsmNodes p WHERE p.id IN :ids"),
    @NamedQuery(name = "PlanetOsmNodes.findByLat", query = "SELECT p FROM PlanetOsmNodes p WHERE p.lat = :lat"),
    @NamedQuery(name = "PlanetOsmNodes.findByLon", query = "SELECT p FROM PlanetOsmNodes p WHERE p.lon = :lon"),
    @NamedQuery(name = "PlanetOsmNodes.findByTags", query = "SELECT p FROM PlanetOsmNodes p WHERE p.tags = :tags")})
public class PlanetOsmNodes implements Serializable,Comparable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "lat")
    private int lat;
    @Basic(optional = false)
    @Column(name = "lon")
    private int lon;
    @Column(name = "tags")
    @Array(databaseType = "text[]")
    private List<String> tags;

    public PlanetOsmNodes() {
    }

    public PlanetOsmNodes(Long id) {
        this.id = id;
    }

    public PlanetOsmNodes(Long id, int lat, int lon) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLat() {
        return lat;
    }

    public void setLat(int lat) {
        this.lat = lat;
    }

    public int getLon() {
        return lon;
    }

    public void setLon(int lon) {
        this.lon = lon;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanetOsmNodes)) {
            return false;
        }
        PlanetOsmNodes other = (PlanetOsmNodes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes[ id=" + id + " ]";
    }
    
    private StringBuilder printTags(){
        StringBuilder sb = new StringBuilder();
        String part1;
        String part2;
        for (int i = 0; i < tags.size(); i= i+2) {
            part1 = tags.get(i);
            part2 = tags.get(i+1);
            sb.append("<tag k=\"");
            sb.append(part1);
            sb.append("\" v=\"");
            sb.append(StringEscapeUtils.escapeXml11(part2));
            sb.append("\"/>\n");
        }
        return sb;
    }
    
    public StringBuilder toXML(){
        StringBuilder sb = new StringBuilder();
        sb.append("<node id=\"");
        sb.append(this.id);
        sb.append("\" version=\"\" timestamp=\"\" uid=\"\" user=\"MLCMP solver\" changeset=\"\" lat=\"");
        sb.append(this.lat);
        sb.append("\" lon=\"");
        sb.append(this.lon);
        if(this.tags.isEmpty())sb.append("\"/>\n");
        else{
            sb.append("\">\n");
            sb.append(printTags());
            sb.append("</node>\n");
        }
        return sb;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof PlanetOsmNodes))return -1;
        if(o == null)return -1;
        PlanetOsmNodes node = (PlanetOsmNodes) o;
        if(this.lon != node.getLon() || this.lat != node.getLat())return 0;
        return 1;
    }
    
}
