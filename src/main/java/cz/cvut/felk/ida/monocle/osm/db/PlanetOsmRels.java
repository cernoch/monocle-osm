/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.cvut.felk.ida.monocle.osm.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

/**
 *
 * @author Pavel Vňuk
 */
@Entity
@Table(name = "planet_osm_rels")
@Struct(name = "REL_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanetOsmRels.findAll", query = "SELECT p FROM PlanetOsmRels p"),
    @NamedQuery(name = "PlanetOsmRels.findById", query = "SELECT p FROM PlanetOsmRels p WHERE p.id = :id"),
    @NamedQuery(name = "PlanetOsmRels.findByIds", query = "SELECT p FROM PlanetOsmRels p WHERE p.id IN :ids"),
    @NamedQuery(name = "PlanetOsmRels.findByWayOff", query = "SELECT p FROM PlanetOsmRels p WHERE p.wayOff = :wayOff"),
    @NamedQuery(name = "PlanetOsmRels.findByRelOff", query = "SELECT p FROM PlanetOsmRels p WHERE p.relOff = :relOff"),
    @NamedQuery(name = "PlanetOsmRels.findByParts", query = "SELECT p FROM PlanetOsmRels p WHERE p.parts = :parts"),
    @NamedQuery(name = "PlanetOsmRels.findByMembers", query = "SELECT p FROM PlanetOsmRels p WHERE p.members = :members"),
    @NamedQuery(name = "PlanetOsmRels.findByTags", query = "SELECT p FROM PlanetOsmRels p WHERE p.tags = :tags"),
    @NamedQuery(name = "PlanetOsmRels.findByPending", query = "SELECT p FROM PlanetOsmRels p WHERE p.pending = :pending")})
public class PlanetOsmRels implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "way_off")
    private Short wayOff;
    @Column(name = "rel_off")
    private Short relOff;
    @Column(name = "parts")
    @Array(databaseType = "bigint[]")
    private List<Long> parts;
    @Column(name = "members")
    @Array(databaseType = "text[]")
    private List<String> members;
    @Column(name = "tags")
    @Array(databaseType = "text[]")
    private List<String> tags;
    @Basic(optional = false)
    @Column(name = "pending")
    private boolean pending;
    @Transient
    private List<Long> ways;
    @Transient
    private HashSet<Long> waysHash;
    @Transient
    private List<Long> nodes;
    @Transient
    private HashSet<Long> nodesHash;
    @Transient
    private String label;
    
    public void parseMembers(){
        ways = new ArrayList<>();
        nodes = new ArrayList<>();
        String member;
        for (int i = 0; i < members.size(); i=i+2) {
            member = members.get(i);
            if(member.charAt(0) == 'n')nodes.add(Long.parseLong(member.substring(1)));
            else ways.add(Long.parseLong(member.substring(1)));
        }
        waysHash = new HashSet<>(ways);
        nodesHash = new HashSet<>(nodes);
    }
    
    public void parseLabel(){
        label = null;
        for (String s : tags) {
            if(s.length()>6){
                if(s.substring(0,4).equals("kct_")){
                    label = s.substring(4);
                    break;
                }
            }
        }
    }

    public String getLabel() {
        return label;
    }

    public List<Long> getWays() {
        return ways;
    }

    public void setWays(List<Long> ways) {
        this.ways = ways;
    }

    public List<Long> getNodes() {
        return nodes;
    }

    public void setNodes(List<Long> nodes) {
        this.nodes = nodes;
    }

    public HashSet<Long> getWaysHash() {
        return waysHash;
    }

    public void setWaysHash(HashSet<Long> waysHash) {
        this.waysHash = waysHash;
    }

    public HashSet<Long> getNodesHash() {
        return nodesHash;
    }

    public void setNodesHash(HashSet<Long> nodesHash) {
        this.nodesHash = nodesHash;
    }
    
    public PlanetOsmRels() {
    }

    public PlanetOsmRels(Long id) {
        this.id = id;
    }

    public PlanetOsmRels(Long id, boolean pending) {
        this.id = id;
        this.pending = pending;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Short getWayOff() {
        return wayOff;
    }

    public void setWayOff(Short wayOff) {
        this.wayOff = wayOff;
    }

    public Short getRelOff() {
        return relOff;
    }

    public void setRelOff(Short relOff) {
        this.relOff = relOff;
    }

    public List<Long> getParts() {
        return parts;
    }

    public void setParts(List<Long> parts) {
        this.parts = parts;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean getPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanetOsmRels)) {
            return false;
        }
        PlanetOsmRels other = (PlanetOsmRels) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels[ id=" + id + " ]";
    }
    
    private StringBuilder printMembers(){
        StringBuilder sb = new StringBuilder();
        String part1;
        String part2;
        for (int i = 0; i < this.members.size(); i= i+2) {
            part1 = this.members.get(i);
            part2 = this.members.get(i+1);
            sb.append("<member type=\"");
            if(part1.charAt(0) == 'n')sb.append("node");
            else sb.append("way");
            sb.append("\" ref=\"");
            sb.append(part1.substring(1));
            sb.append("\" role=\"");
            sb.append(part2);
            sb.append("\"/>\n");
        }
        return sb;
    }
    
    private StringBuilder printTags(){
        StringBuilder sb = new StringBuilder();
        String part1;
        String part2;
        for (int i = 0; i < tags.size(); i= i+2) {
            part1 = tags.get(i);
            part2 = tags.get(i+1);
            sb.append("<tag k=\"");
            sb.append(part1);
            sb.append("\" v=\"");
            sb.append(StringEscapeUtils.escapeXml11(part2));
            sb.append("\"/>\n");
        }
        return sb;
    }
    
    public StringBuilder toXML(){
        StringBuilder sb = new StringBuilder();
        sb.append("<relation id=\"");
        sb.append(this.id);
        sb.append("\" version=\"\" timestamp=\"\" uid=\"\" user=\"MLCMP solver\" changeset=\"\">\n");
        sb.append(printMembers());
        sb.append(printTags());
        sb.append("</relation>\n");
        return sb;
    }

}
