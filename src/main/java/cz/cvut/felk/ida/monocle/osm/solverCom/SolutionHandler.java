/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.solverCom;

import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.fel.rysavpe1.mlcmp.model.Line;
import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.felk.ida.monocle.osm.db.DBHandler;
import cz.cvut.felk.ida.monocle.osm.db.GraphComponents;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.db.WayMap;
import cz.cvut.felk.ida.monocle.osm.logs.LogWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Pavel Vňuk
 */
public class SolutionHandler {
    
    private final DataStore ds;
    private final DBHandler dh;
    
    private final int mode;
    
    private final int FIRSTRUN = 1;
    private final int DIFFRUN = 2;
    
    private ArrayList<Long> changedOsmRels;
    
    public SolutionHandler(int mode){
        this.mode = mode;
        this.dh = DBHandler.getInstance();
        this.ds = DataStore.getInstance();
    }
    
    private void registerProblem(List<Long> ways, List<Long> rels){
        switch(mode){
            case FIRSTRUN: ds.getNewProblems().add(new GraphComponents(ds.getMaxProblemId(), ways, rels));
                    ds.increaseMaxProblemId();return;
            case DIFFRUN: if(ds.getOldProblems().isEmpty()){
                        ds.getNewProblems().add(new GraphComponents(ds.getMaxProblemId(), ways, rels));
                        ds.increaseMaxProblemId();
                    }else{
                        ds.getNewProblems().add(new GraphComponents(ds.getOldProblems().get(0).getId(), ways, rels));
                        ds.getOldProblems().remove(0);
                    }
                    return;
            default:LogWriter.writeIssue("Unknown: Default option in registerProblem.");
        } 
    }
    
    private void mark(Track track, List<Long> wayIds, Line[] lines, Ordering ord){
        Station[] terminus = new Station[2];
        terminus[0] = track.getVertex1();
        terminus[1] = track.getVertex2();
        Long[] terms = new Long[2];
        terms[0] = ds.getStationNodeMap().get(terminus[0]);
        terms[1] = ds.getStationNodeMap().get(terminus[1]);
        
        int start = 0;
        int end = 0;
        
        List<PlanetOsmWays> notSorted;
        HashSet<Long> crossings;
        HashSet<Long> crossTemp;
        
        Line[] trackBound;
        int size = 0;
        int j = 0;
        for (Line line : lines) {
            if(line.containsTrack(track))size++;
        }
        trackBound = new Line[size];
        for (Line line : lines) {
            if(line.containsTrack(track)){
                trackBound[j] = line;
                j++;
            }
        }
        
        if(wayIds.size() == 1){
            PlanetOsmWays way = ds.getWays().get(wayIds.get(0));

            if(ds.getWayTrackMap().get(way.getId()).size() == 1){

                if(terms[0].equals(way.getNodes().get(0))){
                    assignTags(way, 0, 0, track, terminus[0], trackBound, ord);
                }else{
                    assignTags(way, 0, 0, track, terminus[1], trackBound, ord);
                }
            }else{
                Long node;
                for (int i = 0; i < way.getNodes().size(); i++) {
                    node = way.getNodes().get(i);
                    if(node.equals(terms[0]))start = i;
                    if(node.equals(terms[1]))end = i;
                }
                if(end > start){
                    assignTags(way, start, end, track, terminus[0], trackBound, ord);
                }else{
                    assignTags(way, end, start, track, terminus[1], trackBound, ord);
                }
            }
        }else{
            notSorted = new ArrayList<>(wayIds.size());
            crossTemp = new HashSet<>(wayIds.size()+1);
            crossings = new HashSet<>(wayIds.size()-1);

            for (Long wayId : wayIds) {
                notSorted.add(ds.getWays().get(wayId));
            }

            //sorting
            //find crossings
            for (PlanetOsmWays way : notSorted) {
                if(!crossTemp.add(way.getNodes().get(0)))crossings.add(way.getNodes().get(0));
                if(!crossTemp.add(way.getNodes().get(way.getNodes().size()-1)))crossings.add(way.getNodes().get(way.getNodes().size()-1));   
            }
            //find "first" way
            PlanetOsmWays first;
            int remove = 0;
            Long cross = null;
            for (int i = 0; i < notSorted.size(); i++) {
                first = notSorted.get(i);
                if(first.getNodesHash().contains(terms[0])){
                    remove = i;
                    
                    if(ds.getWayTrackMap().get(first.getId()).size() > 1){
                        for (int k = 0; k < first.getNodes().size(); k++) {
                            if(first.getNodes().get(i).equals(terms[0])){
                                start = k;
                                break;
                            }
                        }
                    }
                    

                    if(crossings.contains(first.getNodes().get(0))){
                        cross = first.getNodes().get(0);
                        end = 0;
                        assignTags(first, end, start, track, terminus[1], trackBound, ord);
                    }else{
                        cross = first.getNodes().get(first.getNodes().size()-1);
                        end = first.getNodes().size()-1;
                        assignTags(first, start, end, track, terminus[0], trackBound, ord);
                    }
                    break;
                }
            }
            notSorted.remove(remove);

            while(!notSorted.isEmpty()){
                for (int i = 0; i < notSorted.size(); i++) {
                    first = notSorted.get(i);
                    if(first.getNodesHash().contains(terms[1]) && first.getNodesHash().contains(cross)){
                        remove = i;
                        
                        if(ds.getWayTrackMap().get(first.getId()).size() > 1){
                            for (int k = 0; k < first.getNodes().size(); k++) {
                                if(first.getNodes().get(i).equals(terms[1])){
                                    end = k;
                                    break;
                                }
                            }
                        }    

                        if(crossings.contains(first.getNodes().get(0))){
                            start = 0;
                            assignTags(first, start, end, track, terminus[0], trackBound, ord);
                        }else{
                            start = first.getNodes().size()-1;
                            assignTags(first, end, start, track, terminus[1], trackBound, ord);
                        }

                        break;
                    }
                    if(first.getNodesHash().contains(cross)){
                        remove = i;
                        if(cross.equals(first.getNodes().get(0))){
                            cross = first.getNodes().get(0);
                            assignTags(first, 0, 0, track, terminus[0], trackBound, ord);
                        }else{
                            cross = first.getNodes().get(first.getNodes().size()-1);
                            assignTags(first, 0, 0, track, terminus[1], trackBound, ord);
                        }
                        break;
                    }
                }
                notSorted.remove(remove);
            }
        }
    }
    
    private void assignTags(PlanetOsmWays way, int start, int end, Track track, Station trackStart, Line[] trackBound, Ordering ord){
        List<String> newTags;
        String[] lineOrder = getLineOrder(trackBound, track, trackStart, ord);
        
        PlanetOsmWays newWay;
        
        if(ds.getWayTrackMap().get(way.getId()).size() == 1){
            
            ds.getWayMap().put(way.getId(), new WayMap(way.getId(), new ArrayList<Long>()));
            ds.getWayMap().get(way.getId()).getNewWays().add(way.getId());
            
            newTags = new ArrayList<>();
            for (int i = 0; i < lineOrder.length; i++) {
                newTags.add(lineOrder[i]);
                newTags.add(Integer.toString(i));
            }

            way.addOffsets(newTags);
        }else{
            
            newWay = new PlanetOsmWays(ds.getMinWayId());
            ds.decreaseMinWayId();
            
            newWay.setNodes(way.getNodes().subList(start, end+1));
            newWay.setTags(way.getTags());
            
            newTags = new ArrayList<>();
            for (int i = 0; i < lineOrder.length; i++) {
                newTags.add(lineOrder[i]);
                newTags.add(Integer.toString(i));
            }

            newWay.addOffsets(newTags);
            
            if(!ds.getWayMap().containsKey(way.getId()))ds.getWayMap().put(way.getId(), new WayMap(way.getId(), new ArrayList<Long>()));
            ds.getWayMap().get(way.getId()).getNewWays().add(newWay.getId());
            
        }
    }
    
    private String[] getLineOrder(Line[] lines, Track onTrack, Station trackStart, Ordering ord){
        if(trackStart == null)LogWriter.writeIssue("Unknown: Null track in getLineOrder.");
        String[] lineOrder = new String[lines.length];
        int counter;
        for (int i = 0; i < lineOrder.length; i++) {
            counter = 0;
            for (int j = 0; j < lineOrder.length; j++) {
                if(i == j)continue;
                if(ord.follows(lines[i], lines[j], onTrack, trackStart))counter++;
            }
            lineOrder[counter] = lines[i].getName();
        }
        return lineOrder;
    }
    
    public void backtracing(){
        Line[] lines;
        Track[] tracks;
        List<Long> wayIds;
        HashSet<Long> problemWays;
        List<Long> problemRels;
        
        LogWriter.writeLog("Backtracing has begun.");
        for (MLCMP problem : ds.getOrdering().keySet()) {
            lines = problem.getLines();
            tracks = problem.getEdges();
            problemWays = new HashSet<>();
            problemRels = new ArrayList<>();
            for (Track track : tracks) {
                wayIds = ds.getTrackWayMap().get(track);
                problemWays.addAll(wayIds);
                
                for (PlanetOsmRels rel : ds.getRelations().values()) {
                    for (Long wayId : wayIds) {
                        if(rel.getWaysHash().contains(wayId)){
                            problemRels.add(rel.getId());
                            break;
                        }
                    }
                }
                
                mark(track,wayIds,lines,ds.getOrdering().get(problem));
            }
            
            
            registerProblem(new ArrayList<>(problemWays),problemRels);
        }
        LogWriter.writeLog("Backtracing has ended.");
    }
    
    public void checkSplitWays(){
        ArrayList<Long> splitWays = ds.getSplitWays();
        ArrayList<Long> newWays;
        if(mode == FIRSTRUN){
            for (Long wayId : ds.getWayMap().keySet()) {
                if(ds.getWayMap().get(wayId).getNewWays().size() > 1){
                    splitWays.add(wayId);
                }
            }
            dh.fetchAllOtherRelations();
            for (PlanetOsmRels rel : ds.getRelations().values()) {
                changedOsmRels  = new ArrayList<>(ds.getRelations().size());
                newWays = new ArrayList<>(3*rel.getWays().size());
                for (Long wayId : rel.getWays()) {
                    if(ds.getWayMap().get(wayId).getNewWays().size() > 1){
                        newWays.addAll(ds.getWayMap().get(wayId).getNewWays());
                    }else{
                        newWays.add(wayId);
                    }
                }
                if(newWays.size() > rel.getWays().size()){
                    changedOsmRels.add(rel.getId());
                    rel.setWays(newWays);
                }
            }
            for (PlanetOsmRels rel : ds.getOtherRelations().values()) {
                newWays = new ArrayList<>(3*rel.getWays().size());
                for (Long wayId : rel.getWays()) {
                    if(ds.getWayMap().get(wayId).getNewWays().size() > 1){
                        newWays.addAll(ds.getWayMap().get(wayId).getNewWays());
                    }else{
                        newWays.add(wayId);
                    }
                }
                rel.setWays(newWays);
            }
        }else{
            
        }
    }
    
    public void preparePrintMaps(){
        if(mode == FIRSTRUN){
            ds.getRels_print_M().putAll(ds.getOtherRelations());
            for (Long changedOsmRel : changedOsmRels) {
                ds.getRels_print_M().put(changedOsmRel,ds.getRelations().get(changedOsmRel));
            }
            
            for (WayMap wm : ds.getWayMap().values()) {
                if(wm.getNewWays().size() == 1){
                    ds.getWays_print_M().put(wm.getId(), ds.getWays().get(wm.getId()));
                }else{
                    ds.getWays_print_D().put(wm.getId(), ds.getWays().get(wm.getId()));
                    for (Long wayId : wm.getNewWays()) {
                        ds.getWays_print_C().put(wayId, ds.getWays().get(wayId));
                    }
                }
            }
        }else{
            
        }
    }

    public void sortNewWays(){
        HashMap<Long,WayMap> wayMap = ds.getWayMap();
        Long[] ways;
        for (WayMap wmap : wayMap.values()) {
           ways = new Long[wmap.getNewWays().size()];
           
        }
    }
}
