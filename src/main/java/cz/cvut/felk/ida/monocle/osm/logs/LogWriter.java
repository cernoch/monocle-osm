/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.logs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Pavel Vňuk
 */
public class LogWriter {
    
    private static File log;
    private static File issueLog;
    private static BufferedWriter bwl;
    private static BufferedWriter bwil;
    
    public static void writeLog(String message){
        System.out.println(message);
        if(log != null){
            try {
                bwl.write(message);
            } catch (IOException ex) {
                writeIssue("Critical: Cannot access log file.");
            }
        }
    }
    
    public static void writeIssue(String message){
        System.out.println(message);
        if(issueLog != null){
            try {
                bwil.write(message);
            } catch (IOException ex) {
                writeIssue("Critical: Cannot access issue log file.");
            }
        }
    }

    public static void setLog(File log) {
        LogWriter.log = log;
        try {
            bwl = new BufferedWriter(new FileWriter(log));
        } catch (IOException ex) {
            writeIssue("Critical: Cannot access log file.");
        }
    }

    public static void setIssueLog(File issueLog) {
        LogWriter.issueLog = issueLog;
        try {
            bwil = new BufferedWriter(new FileWriter(issueLog));
        } catch (IOException ex) {
            writeIssue("Critical: Cannot access issue log file.");
        }
    }
}
