/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

/**
 *
 * @author Pavel Vňuk
 */
@Entity
@Table(name = "graph_components")
@Struct(name = "REL_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GraphComponents.findAll", query = "SELECT p FROM GraphComponents p"),
    @NamedQuery(name = "GraphComponents.findById", query = "SELECT p FROM GraphComponents p WHERE p.id = :id")})
public class GraphComponents implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "ways")
    @Array(databaseType = "bigint[]")
    private List<Long> ways;
    @Basic(optional = false)
    @Column(name = "rels")
    @Array(databaseType = "bigint[]")
    private List<Long> rels;
    

    public GraphComponents() {
    }

    public GraphComponents(Long id) {
        this.id = id;
    }

    public GraphComponents(Long id, List<Long> ways, List<Long> rels){
        this.id = id;
        this.ways = ways;
        this.rels = rels;
    }
    
    public Long getId() {
        return id;
    }

    public List<Long> getWays() {
        return ways;
    }
    
    public List<Long> getRels() {
        return rels;
    }

    public void setWays(List<Long> ways) {
        this.ways = ways;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GraphComponents)) {
            return false;
        }
        GraphComponents other = (GraphComponents) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.felk.ida.monocle.osm.db.GraphComponents[ id=" + id + " ]";
    }
    
}
