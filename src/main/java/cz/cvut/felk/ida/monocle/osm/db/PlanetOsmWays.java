/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.cvut.felk.ida.monocle.osm.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang3.StringEscapeUtils;
import org.eclipse.persistence.annotations.Array;
import org.eclipse.persistence.annotations.Struct;

/**
 *
 * @author Pavel Vňuk
 */
@Entity
@Table(name = "planet_osm_ways")
@Struct(name = "REL_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanetOsmWays.findAll", query = "SELECT p FROM PlanetOsmWays p"),
    @NamedQuery(name = "PlanetOsmWays.findById", query = "SELECT p FROM PlanetOsmWays p WHERE p.id = :id"),
    @NamedQuery(name = "PlanetOsmWays.findByIds", query = "SELECT p FROM PlanetOsmWays p WHERE p.id IN :ids"),
    @NamedQuery(name = "PlanetOsmWays.findByNodes", query = "SELECT p FROM PlanetOsmWays p WHERE p.nodes = :nodes"),
    @NamedQuery(name = "PlanetOsmWays.findByTags", query = "SELECT p FROM PlanetOsmWays p WHERE p.tags = :tags"),
    @NamedQuery(name = "PlanetOsmWays.findByPending", query = "SELECT p FROM PlanetOsmWays p WHERE p.pending = :pending")})
public class PlanetOsmWays implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "nodes")
    @Array(databaseType = "bigint[]")
    private List<Long> nodes;
    @Column(name = "tags")
    @Array(databaseType = "text[]")
    private List<String> tags;
    @Basic(optional = false)
    @Column(name = "pending")
    private boolean pending;
    @Transient
    private int offset;
    @Transient
    private HashSet<Long> nodesHash;
    @Transient
    private List<String> offsets;

    public PlanetOsmWays() {
    }

    public PlanetOsmWays(Long id) {
        this.id = id;
    }

    public PlanetOsmWays(Long id, List<Long> nodes, boolean pending) {
        this.id = id;
        this.nodes = nodes;
        this.pending = pending;
    }

    public List<String> getOffsets() {
        return offsets;
    }

    public void setOffsets(List<String> offsets) {
        this.offsets = offsets;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getNodes() {
        return nodes;
    }

    public void setNodes(List<Long> nodes) {
        this.nodes = nodes;
    }

    public List<String> getTags() {
        return tags;
    }

    public void addTags(List<String> tags) {
        if(this.tags == null)this.tags = new ArrayList<>();
        this.tags.addAll(tags);
    }
    
    public void addOffsets(List<String> offsets) {
        if(this.offsets == null)this.offsets = offsets;
        this.offsets.addAll(offsets);
    }

    public boolean getPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }
    
    public void separateOffsetTags(){
        int start = -1;
        for (int i = 0; i < tags.size(); i++) {
            if(tags.get(i).substring(0, 4).equals("off_"))start = i;
            break;
        }
        if(start > -1){
            offsets = tags.subList(start, tags.size());
            tags = tags.subList(0, start);
        }
    }
    
    public void addNodes(List<Long> toAdd, int startIndex){
        nodes.addAll(toAdd.subList(startIndex,toAdd.size()));
        nodesHash.addAll(toAdd.subList(startIndex,toAdd.size()));
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanetOsmWays)) {
            return false;
        }
        PlanetOsmWays other = (PlanetOsmWays) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public HashSet<Long> getNodesHash() {
        return nodesHash;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setNodesHash(HashSet<Long> nodesHash) {
        this.nodesHash = nodesHash;
    }
    
    @Override
    public String toString() {
        return "cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays[ id=" + id + " ]";
    }
    
    private StringBuilder printMembers(){
        StringBuilder sb = new StringBuilder();
        String part1;
        for (Long node : this.nodes) {
            part1 = node.toString();
            sb.append("<nd ref=\"");
            sb.append(part1);
            sb.append("\"/>\n");
        }
        return sb;
    }
    
    private StringBuilder printTags(){
        StringBuilder sb = new StringBuilder();
        String part1;
        String part2;
        for (int i = 0; i < this.tags.size(); i= i+2) {
            part1 = this.tags.get(i);
            part2 = this.tags.get(i+1);
            sb.append("<tag k=\"");
            sb.append(part1);
            sb.append("\" v=\"");
            sb.append(StringEscapeUtils.escapeXml11(part2));
            sb.append("\"/>\n");
        }
        for (int i = 0; i < this.offsets.size(); i= i+2) {
            part1 = this.offsets.get(i);
            part2 = this.offsets.get(i+1);
            sb.append("<tag k=\"");
            sb.append(part1);
            sb.append("\" v=\"");
            sb.append(StringEscapeUtils.escapeXml11(part2));
            sb.append("\"/>\n");
        }
        return sb;
    }
    
    public StringBuilder toXML(){
        StringBuilder sb = new StringBuilder();
        sb.append("<way id=\"");
        sb.append(this.id);
        sb.append("\" version=\"\" timestamp=\"\" uid=\"\" user=\"MLCMP solver\" changeset=\"\">\n");
        sb.append(printMembers());
        sb.append(printTags());
        sb.append("</way>\n");
        return sb;
    }
    
    public void prepareNodeSet(){
        nodesHash = new HashSet<>(nodes.size());
        nodesHash.addAll(nodes);
    }

}
