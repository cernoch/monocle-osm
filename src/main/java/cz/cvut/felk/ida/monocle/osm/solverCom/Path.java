/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.solverCom;

import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Pavel Vňuk
 */
public class Path {
    private final DataStore ds = DataStore.getInstance();
    private final List<PlanetOsmWays> parts;
    private HashSet<Long> crossings;
    private List<PlanetOsmWays> sorted;
    
    public Path(List<Long> partIds){
        parts = new ArrayList<>(partIds.size());
        for (Long partId : partIds) {
            parts.add(ds.getWays().get(partId));
        }
    }
    
    private void findCrossings(){
        if(parts.size() == 1)return;
        HashSet<Long> temp = new HashSet<>(parts.size()*2);
        List<Long> nodeList;
        int lastIndex;
        for (PlanetOsmWays part : parts) {
            nodeList = part.getNodes();
            lastIndex = nodeList.size()-1;
            if(temp.contains(nodeList.get(0))) crossings.add(nodeList.get(0));
            else temp.add(nodeList.get(0));
            
            if(temp.contains(nodeList.get(lastIndex)))crossings.add(nodeList.get(lastIndex));
            else temp.add(nodeList.get(lastIndex));
        }
    }
    
    public List<PlanetOsmWays> getPath(){
        return this.sorted;
    }
    
    public void buildPath(){
        findCrossings();
        sorted = new ArrayList<>(parts.size());
        if(!crossings.isEmpty()){
            PlanetOsmWays selected;
            
            while(!parts.isEmpty()){
                for (int i = 0; i < parts.size(); i++) {
                    selected = parts.get(i);
                    if(!crossings.contains(selected.getNodes().get(0))){
                        sorted.add(selected);
                        parts.remove(i);
                        crossings.remove(selected.getNodes().get(selected.getNodes().size()-1));
                        break;
                    }
                }
            }
        }else{
            sorted = parts;
        }
    }
}
