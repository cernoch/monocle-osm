/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.solverCom;

import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.ContractionEvent;
import cz.cvut.fel.rysavpe1.mlcmp.preprocess.GraphContractionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Pavel Vňuk
 */
public class ContractionListener implements GraphContractionListener{
    private final HashMap<Track,List<Long>> trackWayMap;
    private final HashMap<Long,List<Track>> wayTrackMap;
    private final Track[] tracks = new Track[3];
    Track temp;
        
    public ContractionListener(){
        trackWayMap = DataStore.getInstance().getTrackWayMap();
        wayTrackMap = DataStore.getInstance().getWayTrackMap();
    }
    
    @Override
    public void edgeContracted(ContractionEvent ce) {
        tracks[0] = ce.getNewTrack();
        tracks[1] = ce.getFirstTrack();
        tracks[2] = ce.getSecondTrack();
        
        //sorts position of tracks(one, two) in new track (zero) --- may not be neccessary
        if(tracks[0].getVertex1().equals(tracks[2].getVertex1()) || tracks[0].getVertex1().equals(tracks[2].getVertex2())){
            temp = tracks[1];
            tracks[1] = tracks[2];
            tracks[2] = temp;
        }
        
        if(trackWayMap.get(tracks[1]).size() == 1 && trackWayMap.get(tracks[2]).size() == 1){
            trackWayMap.put(tracks[0], trackWayMap.get(tracks[1]));
            if(!trackWayMap.get(tracks[1]).get(0).equals(trackWayMap.get(tracks[2]).get(0))){
                trackWayMap.get(tracks[0]).add(trackWayMap.get(tracks[2]).get(0));
            }
        }else{
            //indicates that track connected reversed in new track
            boolean reversedFirst = false;
            boolean reversedSecond = false;
            
            if(tracks[0].getVertex1().equals(tracks[1].getVertex2()))reversedFirst = true;
            if(tracks[0].getVertex2().equals(tracks[2].getVertex1()))reversedSecond = true;
            List<Long> ids;
            List<Long> idsFirst;
            List<Long> idsSecond;
            
            if(!reversedFirst)ids = trackWayMap.get(tracks[1]);
            else{
                ids = new ArrayList<>();
                idsFirst = trackWayMap.get(tracks[1]);
                for (int i = idsFirst.size()-1; i >= 0 ; i++) {
                    ids.add(idsFirst.get(i));
                }
            }
            idsSecond = trackWayMap.get(tracks[2]);
            if(!reversedSecond){
                for (int i = 1; i < idsSecond.size(); i++) {
                    ids.add(idsSecond.get(i));
                }
            }else{
                for (int i = idsSecond.size()-2; i >= 0 ; i++) {
                    ids.add(idsSecond.get(i));
                }
            }
            trackWayMap.put(tracks[0], ids);
        }
        trackWayMap.remove(tracks[1]);
        trackWayMap.remove(tracks[2]);
        for (Long wayId : trackWayMap.get(tracks[0])) {
            wayTrackMap.get(wayId).remove(tracks[1]);
            wayTrackMap.get(wayId).remove(tracks[2]);
            wayTrackMap.get(wayId).add(tracks[0]);
        }
    }
    
}
