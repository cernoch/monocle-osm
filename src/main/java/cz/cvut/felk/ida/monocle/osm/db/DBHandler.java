/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.cvut.felk.ida.monocle.osm.db;

import cz.cvut.felk.ida.monocle.osm.configs.ConfigLoader;
import cz.cvut.felk.ida.monocle.osm.logs.LogWriter;
import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

/**
 *
 * @author Pavel Vňuk
 */
public class DBHandler{
    @PersistenceUnit
    EntityManagerFactory emfOSM;
    EntityManagerFactory emfGraph;
    EntityManager em;
    
    private static DBHandler instance;
    
    private final DataStore ds;
       
    private DBHandler(){
        ds = DataStore.getInstance();
        emfOSM = Persistence.createEntityManagerFactory("cz.cvut.felk.ida.OSM-persistence");
        emfGraph = Persistence.createEntityManagerFactory("cz.cvut.felk.ida.GRAPH-persistence");
    }
    
    public static DBHandler getInstance(){
        if(instance == null)instance = new DBHandler();
        return instance;
    }
    
    private void phase1(){
        Map<Long,PlanetOsmRels> relations = ds.getRelations();
        Map<Long,PlanetOsmWays> ways = ds.getWays();
        
        NativeQueryCreator nqc = new NativeQueryCreator();
        nqc.configure();
        String nativeQ = nqc.generateOSMQuery().toString();
        
        em = emfOSM.createEntityManager();
        em.getTransaction().begin();
        
        List<PlanetOsmRels> dbRels = (List<PlanetOsmRels>) em.createNativeQuery(nativeQ,PlanetOsmRels.class).getResultList();
        
        for (PlanetOsmRels dbRel : dbRels) {
            relations.put(dbRel.getId(),dbRel);
            dbRel.parseMembers();
            dbRel.parseLabel();
            for (Long way_id : dbRel.getWays()) {
                ways.put(way_id, null);
            }
        }
        
        em.getTransaction().commit();
        em.close();
        
        LogWriter.writeLog("Rels exctracted = " + ds.getRelations().size()  + ".");
    }
    
    private void phase2(){
        Map<Long,PlanetOsmWays> ways = ds.getWays();
        Map<Long,PlanetOsmNodes> nodes = ds.getNodes();
        
        List<List<Long>> split = spliter(ways.keySet());
        
        LogWriter.writeLog("Number of ways to extract =" + ways.size() + ".");
                
        for (List<Long> list : split) {
            em = emfOSM.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<PlanetOsmWays> q = em.createNamedQuery("PlanetOsmWays.findByIds", PlanetOsmWays.class);
            q.setParameter("ids", list);
            List<PlanetOsmWays> result = q.getResultList();
            for (PlanetOsmWays res : result) {
                res.prepareNodeSet();
                ways.put(res.getId(), res);
                for (Long node_id : res.getNodes()) {
                    nodes.put(node_id,null);
                }
            }
            em.getTransaction().commit();
            em.close();
        }
        
        LogWriter.writeLog("Ways exctracted = " + ways.size()  + ".");
    }
    
    private void phase3(){
        Map<Long,PlanetOsmNodes> nodes = ds.getNodes();
        
        List<List<Long>> split = spliter(nodes.keySet());
        
        LogWriter.writeLog("Number of nodes to extract =" + nodes.size() + ".");
               
        for (List<Long> list : split) {
            em = emfOSM.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<PlanetOsmNodes> q = em.createNamedQuery("PlanetOsmNodes.findByIds", PlanetOsmNodes.class);
            q.setParameter("ids", list);
            List<PlanetOsmNodes> result = q.getResultList();
            for (PlanetOsmNodes res : result) {
                nodes.put(res.getId(), res);
            }
            em.getTransaction().commit();
            em.close();
        }
        
        LogWriter.writeLog("Nodes exctracted = " + nodes.size()  + ".");
    }
    
    private void phase1diff(){
        Map<Long,PlanetOsmRels> fetchedRelations = ds.getFetchedRelations();
        
        List<List<Long>> split = spliter(fetchedRelations.keySet());
        
        for (List<Long> list : split) {
            em = emfOSM.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<PlanetOsmRels> q = em.createNamedQuery("PlanetOsmRels.findByIds", PlanetOsmRels.class);
            q.setParameter("ids", list);
            List<PlanetOsmRels> result = q.getResultList();
            for (PlanetOsmRels res : result) {
                fetchedRelations.put(res.getId(), res);
                res.parseMembers();
                res.parseLabel();
            }
            em.getTransaction().commit();
            em.close();
        }
        
        LogWriter.writeLog("Rels exctracted = " + ds.getRelations().size()  + ".");
    }
    
    private void phase2diff(){
        Map<Long,PlanetOsmWays> fetchedWays = ds.getFetchedWays();
        
        List<List<Long>> split = spliter(fetchedWays.keySet());
        
        LogWriter.writeLog("Number of ways to extract =" + fetchedWays.size() + ".");
                
        for (List<Long> list : split) {
            em = emfOSM.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<PlanetOsmWays> q = em.createNamedQuery("PlanetOsmWays.findByIds", PlanetOsmWays.class);
            q.setParameter("ids", list);
            List<PlanetOsmWays> result = q.getResultList();
            for (PlanetOsmWays res : result) {
                res.prepareNodeSet();
                fetchedWays.put(res.getId(), res);
            }
            em.getTransaction().commit();
            em.close();
        }
        
        LogWriter.writeLog("Ways exctracted = " + fetchedWays.size()  + ".");
    }
    
    private void phase3diff(){
        Map<Long,PlanetOsmNodes> fetchedNodes = ds.getFetchedNodes();
        
        List<List<Long>> split = spliter(ds.getNodes_M().keySet());
        
        LogWriter.writeLog("Number of nodes to extract =" + fetchedNodes.size() + ".");
               
        for (List<Long> list : split) {
            em = emfOSM.createEntityManager();
            em.getTransaction().begin();
            TypedQuery<PlanetOsmNodes> q = em.createNamedQuery("PlanetOsmNodes.findByIds", PlanetOsmNodes.class);
            q.setParameter("ids", list);
            List<PlanetOsmNodes> result = q.getResultList();
            for (PlanetOsmNodes res : result) {
                fetchedNodes.put(res.getId(), res);
            }
            em.getTransaction().commit();
            em.close();
        }
        
        LogWriter.writeLog("Nodes exctracted = " + fetchedNodes.size()  + ".");
    }
    
    public void phase4diff(){
        HashMap<Long,PlanetOsmWays> fetchedWays = ds.getFetchedWays();
        HashMap<Long,PlanetOsmNodes> fetched_nodes = ds.getNodes_M();
        NativeQueryCreator nqc = new NativeQueryCreator();
        nqc.configure();
        String nativeQ;
        
        em = emfOSM.createEntityManager();
        em.getTransaction().begin();
        
        List<PlanetOsmWays> waysExtracted;
        
        for (Long nodeId : fetched_nodes.keySet()) {
            nativeQ = nqc.generateLikeQuery(nodeId.toString(),"planet_osm_ways","nodes").toString();
            waysExtracted = em.createNativeQuery(nativeQ,PlanetOsmWays.class).getResultList();
            for (PlanetOsmWays way : waysExtracted) {
                fetchedWays.put(way.getId(), way);
            }
        }
        
        em.getTransaction().commit();
        em.close();
        
        LogWriter.writeLog("Ways exctracted = " + fetchedWays.size()  + ".");
    }
    
    public void fetchAllOtherRelations(){
        HashMap<Long,PlanetOsmRels> otherRelations = ds.getOtherRelations();
        
        NativeQueryCreator nqc = new NativeQueryCreator();
        nqc.configure();
        String nativeQ;
        
        em = emfOSM.createEntityManager();
        em.getTransaction().begin();
        
        List<PlanetOsmRels> relsExtracted;
        
        for (Long wayId : ds.getSplitWays()) {
            nativeQ = nqc.generateLikeQuery("w"+wayId,"planet_osm_rels","members").toString();
            relsExtracted = em.createNativeQuery(nativeQ,PlanetOsmRels.class).getResultList();
            for (PlanetOsmRels rel : relsExtracted) {
                if(ds.getRelations().containsKey(rel.getId()))continue;
                rel.parseLabel();
                rel.parseMembers();
            }
        }
        
        em.getTransaction().commit();
        em.close();
        
        LogWriter.writeLog("Other relations exctracted = " + otherRelations.size()  + ".");
    }
    
    public void fetchDBDataFT(){
        phase1();
        phase2();
        phase3();
    }
    
    public void fetchDBDataDiff(){
        phase1diff();
        phase2diff();
        phase3diff();
    }
    
    public void fetchGraphComponents(){
        ArrayList<GraphComponents> gcs = ds.getOldProblems();
        
        em = emfGraph.createEntityManager();
        em.getTransaction().begin();
        
        gcs.addAll(em.createNamedQuery("GraphComponents.findAll", GraphComponents.class).getResultList());
        
        em.getTransaction().commit();
        em.close();
        
        for (GraphComponents gc : gcs) {
            ds.getOldRels().addAll(gc.getRels());
        }
        
        LogWriter.writeLog("Graph components exctracted = " + gcs.size()  + ".");
    }
    
    public void saveGraphComponents(int mode){
        if(mode == 1){
            for (GraphComponents gc : ds.getNewProblems()) {
                em = emfGraph.createEntityManager();
                em.getTransaction().begin();
                em.persist(gc);
                em.getTransaction().commit();
                em.close();
            }
        }else{
            GraphComponents toMerge;
            for (GraphComponents gc : ds.getNewProblems()) {
                em = emfGraph.createEntityManager();
                em.getTransaction().begin();
                toMerge = em.find(GraphComponents.class, gc.getId());
                if(toMerge != null)em.merge(gc);
                em.getTransaction().commit();
                em.close();
            }
        }
    }
    
    public void saveWayMaps(int mode){
        if(mode == 1){
            for (WayMap wm : ds.getWayMap().values()) {
                em = emfGraph.createEntityManager();
                em.getTransaction().begin();
                em.persist(wm);
                em.getTransaction().commit();
                em.close();
            }
        }else{
            WayMap toMerge;
            for (WayMap wm : ds.getWayMap().values()) {
                em = emfGraph.createEntityManager();
                em.getTransaction().begin();
                toMerge = em.find(WayMap.class, wm.getId());
                if(toMerge != null)em.merge(wm);
                em.getTransaction().commit();
                em.close();
            }
        }
    }
    
    public void fetchWayMap(){
        Map<Long,WayMap> oldWayMap = ds.getOldWayMap();
        Map<Long,Long> reverseWayMap = ds.getReverseWayMap();
        List<WayMap> results;
        
        em = emfGraph.createEntityManager();
        em.getTransaction().begin();
        
        results = em.createNamedQuery("WayMap.findAll", WayMap.class).getResultList();
        
        em.getTransaction().commit();
        em.close();
        
        for (WayMap result : results) {
            oldWayMap.put(result.getId(), result);
            for (Long newWayId : result.getNewWays()) {
                reverseWayMap.put(newWayId, result.getId());
            }
        }
        
        LogWriter.writeLog("WayMaps exctracted = " + results.size()  + ".");
    }
    
    public void closeOSMFactory(){
        emfOSM.close();
    }
    
    public void closeGraphFactory(){
        emfGraph.close();
    }
    
    private List<List<Long>> spliter(Collection<Long> toSplit){
        List<List<Long>> split = new ArrayList<>();
        List<Long> part = new ArrayList<>(1000);
        int counter = 0;
        for (Long id : toSplit) {
            if(counter%1000 == 0 && counter > 0){
                split.add(part);
                part = new ArrayList<>(1000);
            }
            part.add(id);
            counter++;
        }
        split.add(part);
        return split;
    }
    
}
