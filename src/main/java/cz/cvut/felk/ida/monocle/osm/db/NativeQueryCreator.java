/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.db;

import java.util.HashSet;

/**
 *
 * @author Pavel Vňuk
 */
public class NativeQueryCreator {
    
    private HashSet<String> colors;
    private HashSet<String> kctTags;
    
    private boolean allColors;
    private boolean allKct;
    
    private final String OPERATOR = "cz:KČT";
    private final String KCT = "kct_";
    private final String T = "tags";
    private final String ORB = "[";
    private final String CRB = "]";
    private final HashSet<String> validColors;
    private final HashSet<String> validKctTags;
    
    public NativeQueryCreator(){
        validColors = new HashSet<>(4);
        validKctTags = new HashSet<>(11);
        prepareValidSets();
    }
    
    private void prepareValidSets(){
        validColors.add("red");
        validColors.add("green");
        validColors.add("yellow");
        validColors.add("blue");
        
        validKctTags.add("major");
        validKctTags.add("local");
        validKctTags.add("learning");
        validKctTags.add("ruin");
        validKctTags.add("peak");
        validKctTags.add("spring");
        validKctTags.add("interesting_object");
        validKctTags.add("horse");
        validKctTags.add("ski");
        validKctTags.add("bicycle");
        validKctTags.add("wheelchair");
    }
    
    
    /*
    *   TODO ----- change to be dependent on config file
    */
    private void loadKctTags(){
        kctTags = new HashSet<>(5);
        kctTags.add("ski");
        kctTags.add("bicycle");
        kctTags.add("major");
        kctTags.add("local");
        kctTags.add("learning");
    }

    public boolean isAllColors() {
        return allColors;
    }

    public void setAllColors(boolean allColors) {
        this.allColors = allColors;
    }

    public boolean isAllKct() {
        return allKct;
    }

    public void setAllKct(boolean allKct) {
        this.allKct = allKct;
    }
    
    public void configure(){
        setAllColors(true);
        setAllKct(false);
        loadKctTags();
    }
    
    public StringBuilder generateOSMQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("select * from planet_osm_rels where ");
        if(allKct){
            sb.append("\'").append(OPERATOR).append("\'").append(" LIKE ANY (").append(T).append(")");
        }else if(allColors){
            sb.append("(");
            int it = 0;
            for (String tag : kctTags) {
                it++;
                for (int i = 3; i < 19; i=i+2) {
                    sb.append("(");
                    sb.append(T).append(ORB).append(i).append(CRB).append(" LIKE \'").append(KCT).append("%").append("\'");
                    sb.append(" AND ");
                    sb.append(T).append(ORB).append(i+1).append(CRB).append("=\'").append(tag).append("\'");
                    sb.append(")");
                    if(i<17)sb.append(" OR ");
                }
                if(it<kctTags.size())sb.append(" OR ");
            }
            sb.append(");");
        }else{
            
        }
        
        return sb;
    }
    
    public StringBuilder generateLikeQuery(String id, String table, String column){
        StringBuilder sb = new StringBuilder();
        sb.append("select * from ");
        sb.append(table);
        sb.append(" where ");
        sb.append("\'").append(id).append("\'").append(" LIKE ANY (").append(column).append(")");
        return sb;
    }
}
