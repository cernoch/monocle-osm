/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.xml;

import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.OneUseXMLParser;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import static cz.cvut.felk.ida.monocle.osm.xml.OSMXML2.*;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author Pavel Vňuk
 */
public class WayParser extends OneUseXMLParser{
    private final DataStore ds;
    private PlanetOsmWays way;
    private List<String> tags;
    private List<Long> nodes;
    
    private String mode;
    
    public WayParser(){
        ds = DataStore.getInstance();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes ats) throws SAXException{
        if (localName.equals(NODE_REF_TAG)){
                nodes.add(Long.parseLong(ats.getValue(REF_ATTR)));
                return;
        }
        
        if (localName.equals(TAG_TAG)){
            tags.add(ats.getValue(KEY_ATTR));
            tags.add(ats.getValue(VALUE_ATTR));
            return;
        }

        if (localName.equals(WAY_TAG)){
            way = new PlanetOsmWays(Long.parseLong(ats.getValue(ID_ATTR)));
            tags = new ArrayList<>();
            nodes = new ArrayList<>();
            return;
        }
        
        if(localName.equals(MODIFY_TAG) || localName.equals(CREATE_TAG) || localName.equals(DELETE_TAG)){
            mode = localName;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException{
        if (way != null && localName.equals(WAY_TAG)){
            way.setNodes(nodes);
            way.addTags(tags);
            
            switch(mode){
                case CREATE_TAG:ds.getWays_C().put(way.getId(), way);break;
                case MODIFY_TAG:ds.getWays_M().put(way.getId(), way);break;
                case DELETE_TAG:ds.getWays_D().put(way.getId(), way);break;
                default:
            }

            nodes = null;
            tags = null;
            way = null;
        }
    }
}
