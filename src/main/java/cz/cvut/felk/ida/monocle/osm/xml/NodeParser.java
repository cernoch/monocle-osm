/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.xml;

import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.OneUseXMLParser;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import static cz.cvut.felk.ida.monocle.osm.xml.OSMXML2.*;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author Pavel Vňuk
 */
public class NodeParser extends OneUseXMLParser{
    private final DataStore ds;
    private PlanetOsmNodes node;
    private List<String> tags;
    
    private String mode; 
    
    public NodeParser(){
        ds = DataStore.getInstance();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes ats) throws SAXException{
        if(node != null && localName.equals(TAG_TAG)){
            tags.add(ats.getValue(KEY_ATTR));
            tags.add(ats.getValue(VALUE_ATTR));
            return;
        }

	if(localName.equals(NODE_TAG)){
            node = new PlanetOsmNodes(Long.parseLong(ats.getValue(ID_ATTR)),Integer.parseInt(ats.getValue(LATITUDE_ATTR)),Integer.parseInt(ats.getValue(LONGITUDE_ATTR)));
            tags = new ArrayList<>();
            return;
        }

        if(localName.equals(MODIFY_TAG) || localName.equals(CREATE_TAG) || localName.equals(DELETE_TAG)){
            mode = localName;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException{
        if (node != null && localName.equals(NODE_TAG)){
            node.setTags(tags);
            switch(mode){
                case CREATE_TAG:ds.getNodes_C().put(node.getId(), node);break;
                case MODIFY_TAG:ds.getNodes_M().put(node.getId(), node);break;
                case DELETE_TAG:ds.getNodes_D().put(node.getId(), node);break;
                default:
            }
            tags = null;
            node = null;
        }
    }
}
