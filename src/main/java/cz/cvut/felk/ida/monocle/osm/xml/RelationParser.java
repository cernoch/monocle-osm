/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.xml;

import static cz.cvut.fel.rysavpe1.mlcmp.input.osm.OSMXML.*;
import cz.cvut.fel.rysavpe1.mlcmp.input.osm.OneUseXMLParser;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import static cz.cvut.felk.ida.monocle.osm.xml.OSMXML2.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *
 * @author Pavel Vňuk
 */
public class RelationParser extends OneUseXMLParser{
    private final DataStore ds;
    private PlanetOsmRels relation;
    private List<String> tags;
    private List<Long> parts;
    private List<String> members;
    
    private List<Long> nodes;
    private List<Long> ways;
    
    private String mode;
    
    private Long id;
    
    public RelationParser(){
        ds = DataStore.getInstance();
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes ats) throws SAXException{
        if(localName.equals(MEMBER_TAG)){
            if(ats.getValue(TYPE_ATTR).equals(WAY_TAG)){
                id = Long.parseLong(ats.getValue(REF_ATTR));
                ways.add(id);
                parts.add(id);
                members.add("w"+id);
                members.add(ats.getValue(ROLE_ATTR));
            }else{
                id = Long.parseLong(ats.getValue(REF_ATTR));
                nodes.add(id);
                parts.add(id);
                members.add("n"+id);
                members.add(ats.getValue(ROLE_ATTR));
            }
            return;
        }

        if(localName.equals(TAG_TAG)){
            tags.add(ats.getValue(KEY_ATTR));
            tags.add(ats.getValue(VALUE_ATTR));
            return;
        }

        if(localName.equals(RELATION_TAG)){
                relation = new PlanetOsmRels(Long.parseLong(ats.getValue(ID_ATTR)));
                ways = new ArrayList<>();
                tags = new ArrayList<>();
                members = new ArrayList<>();
                parts = new ArrayList<>();
        }
        
        if(localName.equals(MODIFY_TAG) || localName.equals(CREATE_TAG) || localName.equals(DELETE_TAG)){
            mode = localName;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException{
        if (localName.equals(RELATION_TAG)){
            relation.setMembers(members);
            relation.setNodes(nodes);
            relation.setParts(parts);
            relation.setWays(ways);
            relation.setTags(tags);
            relation.setWaysHash(new HashSet<>(ways));
            relation.setNodesHash(new HashSet<>(nodes));
            relation.parseLabel();
            
            switch(mode){
                case CREATE_TAG:ds.getRels_C().put(relation.getId(), relation);break;
                case MODIFY_TAG:ds.getRels_M().put(relation.getId(), relation);break;
                case DELETE_TAG:ds.getRels_D().put(relation.getId(), relation);break;
                default:
            }
            
            members = null;
            nodes = null;
            parts = null;
            ways = null;
            tags = null;
        }
    }
}
