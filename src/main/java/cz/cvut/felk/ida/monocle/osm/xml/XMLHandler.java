/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.xml;

import cz.cvut.felk.ida.monocle.osm.logs.LogWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openstreetmap.partrksplitter.input.XmlParser;
import org.xml.sax.SAXException;

/**
 *
 * @author Pavel Vňuk
 */
public class XMLHandler{
    private final Path diff;
    
    public XMLHandler(Path diff){
        this.diff = diff;
    }
    
    private void runParser(XmlParser parser) throws SAXException, IOException{
        parser.parse(diff);
    }

    private void phase1() throws SAXException, IOException{
        RelationParser parser = new RelationParser();
        runParser(parser);
    }

    private void phase2() throws SAXException, IOException{
        WayParser parser = new WayParser();
        runParser(parser);
    }

    private void phase3() throws SAXException, IOException{
        NodeParser parser = new NodeParser();
        runParser(parser);
    }
    
    public void parseDiff(){
        try {
            phase1();
        } catch (SAXException ex) {
            LogWriter.writeIssue("Critical: SAX parser exception during relation reading phase.");
        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: I/O exception during relation reading phase.");
        }
        LogWriter.writeLog("All relations succesfully parsed.");
        try {
            phase2();
        } catch (SAXException ex) {
            LogWriter.writeIssue("Critical: SAX parser exception during way reading phase.");
        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: I/O exception during way reading phase.");
        }
        LogWriter.writeLog("All ways succesfully parsed.");
        try {
            phase3();
        } catch (SAXException ex) {
            LogWriter.writeIssue("Critical: SAX parser exception during node reading phase.");
        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: I/O exception during node reading phase.");
        }
        LogWriter.writeLog("All nodes succesfully parsed.");
    }
}
