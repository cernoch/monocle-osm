/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.data;

import cz.cvut.felk.ida.monocle.osm.db.DBHandler;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.db.WayMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Class used for preparing data from DIFFs for solver.
 * Prepares data for {@link cz.cvut.felk.ida.monocle.osm.solverCom.SolverLoader}
 * Uses {@link cz.cvut.felk.ida.monocle.osm.db.DBHandler} for populating
 * used maps in {@link cz.cvut.felk.ida.monocle.osm.data.DataStore}
 * @author Pavel Vňuk
 */
public class DifferenceChecker {
    private final DataStore ds;
    private final DBHandler dh;
    
    /**
     * Creates instance of DifferenceChecker.
     */
    public DifferenceChecker(){
        ds = DataStore.getInstance();
        dh = DBHandler.getInstance();
    }
    
    /**
     * Checks if Relation {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_C}
     * has {@link cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels#label}.
     * Then either sets all ways of this Relation as {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#problematicWays}
     * or moves Relation to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_print_C}.
     */
    private void checkCreatedRelsPart1(){
        HashSet<Long> problematicWays = ds.getProblematicWays();
        for (PlanetOsmRels rel : ds.getRels_C().values()) {
            if(rel.getLabel() == null){
                ds.getRels_print_C().put(rel.getId(), rel);
            }else{
                problematicWays.addAll(rel.getWays());
            }
        }
        for (PlanetOsmRels rel : ds.getRels_print_C().values()) {
            ds.getRels_C().remove(rel.getId());
        }
    }
    
    /**
     * Checks if Relation {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_D}
     * has {@link cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels#label}.
     * Then either sets all ways of this Relation as {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#problematicWays}
     * or moves Relation to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_print_D}.
     */
    private void checkDeletedRelsPart1(){
        HashSet<Long> problematicWays = ds.getProblematicWays();
        for (PlanetOsmRels rel : ds.getRels_D().values()) {
            if(rel.getLabel() == null){
                ds.getRels_print_D().put(rel.getId(), rel);
            }else{
                problematicWays.addAll(rel.getWays());
            }
        }
        for (PlanetOsmRels rel : ds.getRels_print_D().values()) {
            ds.getRels_D().remove(rel.getId());
        }
    }
    
    /**
     * Checks if Relation {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_M} 
     * exists in {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#oldRels} or
     * has {@link cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels#label}. 
     * Then either sets all ways of this Relation as {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#problematicWays}
     * or moves Relation to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_print_C}.
     */
    private void checkModifiedRelsPart1(){
        HashSet<Long> oldRels = ds.getOldRels();
        
        for (PlanetOsmRels rel : ds.getRels_M().values()) {
            if(oldRels.contains(rel.getId()) || rel.getLabel() != null){
                ds.getFetchedRelations().put(rel.getId(), null);
            }else{
                ds.getRels_print_M().put(rel.getId(), rel);
            }
        }
        for (PlanetOsmRels rel : ds.getRels_print_M().values()) {
            ds.getRels_M().remove(rel.getId());
        }
    }
    
    /**
     * Compares two rels (old and modified/new).
     * Relevant - less/more ways, change in "kct_" TAG
     * Irrelevant - change in nodes, irrelevant tags
     * @param modified
     * @param old
     * @return 0 for relevant change, 1 for irrelevant change
     */
    private int relComparator(PlanetOsmRels modified , PlanetOsmRels old){
        HashSet<Long> oldWays = old.getWaysHash();
        HashSet<Long> modWays = modified.getWaysHash();
        if(modified.getLabel() == null || old.getLabel() == null)return 0;
        if(!modified.getLabel().equals(old.getLabel()))return 0;
        if(modWays.size() == oldWays.size()){
            for (Long oldWay : oldWays) {
                if(!modWays.contains(oldWay))return 0;
            }
            return 1;
        }else return 0;
    }
    
    /**
     * Does either nothing when {@link DifferenceChecker#relComparator(cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels, cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels)}
     * method returns 0 for compared Relations (old, modified/new) or
     * moves modified/new Relation to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_print_M} 
     * and removes it from {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#rels_M} afterwards.
     */
    private void checkModifiedRelsPart2(){
        HashMap<Long,PlanetOsmRels> fetchedRels = ds.getFetchedRelations();
        HashMap<Long,PlanetOsmRels> rels_M = ds.getRels_M();
        
        for (PlanetOsmRels rel : rels_M.values()) {
            switch(relComparator(rel,fetchedRels.get(rel.getId()))){
                case 0: break;
                default: ds.getRels_print_M().put(rel.getId(), rel);
            }
        }
        for (PlanetOsmRels rel : ds.getRels_print_M().values()) {
            ds.getRels_M().remove(rel.getId());
        }
    }
    
    /**
     * Does either nothing when {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#oldWayMap}
     * do not contain id of way from {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#ways_M}
     * or moves modified/new Way to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#ways_print_M} 
     * and removes it from {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#ways_M} afterwards.
     */
    private void checkModifiedWaysPart1(){
        HashMap<Long,WayMap> oldWays = ds.getOldWayMap();
        for (PlanetOsmWays way : ds.getWays_M().values()) {
            if(oldWays.containsKey(way.getId())){
                //put all new ways to fetch list
                for (Long newWay : ds.getOldWayMap().get(way.getId()).getNewWays()) {
                    ds.getFetchedWays().put(newWay, null);
                }
            }else{
                ds.getWays_print_M().put(way.getId(), way);
            }
        }
        for (PlanetOsmWays way : ds.getWays_print_M().values()) {
            ds.getWays_M().remove(way.getId());
        }
    }
    
    /**
     * Compares two ways (old and modified/new).
     * @param modified
     * @param old
     * @return 0 for relevant change, 1 for irrelevant change
     */
    private int wayComparator(PlanetOsmWays modified, PlanetOsmWays old){
        List<Long> oldNodes = old.getNodes();
        List<Long> modNodes = modified.getNodes();
        if(modNodes.size() == oldNodes.size()){
            for (int i = 0; i < oldNodes.size(); i++) {
                if(!modNodes.get(i).equals(oldNodes.get(i)))return 0;
            }
            return 1;
        }else return 0;
    }
    
    /**
     * Does either nothing when {@link DifferenceChecker#wayComparator(cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays, cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays) }
     * method returns 0 for compared Ways (old, modified/new) or
     * moves modified/new Way to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#ways_print_M} 
     * and removes it from {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#ways_M} afterwards.
     */
    private void checkModifiedWaysPart2(){
        HashMap<Long,PlanetOsmWays> fetchedWays = ds.getFetchedWays();
        HashMap<Long,PlanetOsmWays> ways_M = ds.getWays_M();
        
        for (PlanetOsmWays way : ways_M.values()) {
            switch(wayComparator(way,fetchedWays.get(way.getId()))){
                case 0: break;
                default: ds.getWays_print_M().put(way.getId(), way);
            }
        }
        for (PlanetOsmWays way : ds.getWays_print_M().values()) {
            ds.getWays_M().remove(way.getId());
        }
    }
    
    /**
     * Compares two Nodes (old, modified/new),
     * then either does nothing (case 0 - relevant change) or
     * moves modified/new Node to {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#nodes_print_M} 
     * and removes it from {@link cz.cvut.felk.ida.monocle.osm.data.DataStore#nodes_M} afterwards. 
     */
    private void checkModifiedNodesPart1(){
        HashMap<Long,PlanetOsmNodes> fetchedNodes = ds.getFetchedNodes();
        HashMap<Long,PlanetOsmNodes> nodes_M = ds.getFetchedNodes();
        
        for (PlanetOsmNodes node : nodes_M.values()) {
            switch(node.compareTo(fetchedNodes.get(node.getId()))){
                case 0: break;
                default: ds.getNodes_print_M().put(node.getId(), node);break;
            }
        }
        for (PlanetOsmNodes node : ds.getNodes_print_M().values()) {
            nodes_M.remove(node.getId());
            fetchedNodes.remove(node.getId());
        }
    }
    
    private void checkModifiedNodesPart2(){
        
    }
    
    private void applyChanges(){
        
    }
    
    private void presolving(){
        //methods for first sorting of diff maps
        checkCreatedRelsPart1();
        checkDeletedRelsPart1();
        checkModifiedRelsPart1();
        checkModifiedWaysPart1();
        dh.fetchDBDataDiff();
        checkModifiedNodesPart1();
        
        
        checkModifiedRelsPart2();
        checkModifiedWaysPart2();
        checkModifiedNodesPart2();
    }
    
    private void reconstruction(){
        
    }
    
    private void postsolving(){
        
    }
    
}
