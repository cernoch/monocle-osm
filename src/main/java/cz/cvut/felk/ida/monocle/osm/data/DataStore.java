/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.data;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.model.Station;
import cz.cvut.fel.rysavpe1.mlcmp.model.Track;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.felk.ida.monocle.osm.db.GraphComponents;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.db.WayMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pavel Vňuk
 */
public class DataStore {
    
    private static DataStore instance;
    
    /*
    * Maps used to save OSM entities by {@code DBHandler}. 
    */
    private final HashMap<Long,PlanetOsmRels> relations;
    private final HashMap<Long,PlanetOsmWays> ways;
    private final HashMap<Long,PlanetOsmNodes> nodes;
    
    /*
    * Relations, that are not related to solution,
    * but contain ways related to solution.
    */
    private final HashMap<Long,PlanetOsmRels> otherRelations;
    
    private final HashMap<Long,PlanetOsmRels> fetchedRelations;
    private final HashMap<Long,PlanetOsmWays> fetchedWays;
    private final HashMap<Long,PlanetOsmNodes> fetchedNodes;
    
    private final HashSet<Long> problematicWays;
    
    /*
    * Solution entities and last used maxWayId for creation of new ones
    */
    private final HashSet<Long> oldRels;
    private final ArrayList<GraphComponents> oldProblems;
    private final ArrayList<GraphComponents> newProblems;
    
    /*
    * Ways, that were split into smaller parts during solving.
    * Used while finding unrelated rels in DB.
    */
    private final ArrayList<Long> splitWays;
    
    private final HashMap<Long,WayMap> wayMap;
    private final HashMap<Long,WayMap> oldWayMap;
    private final HashMap<Long,Long> reverseWayMap;
    
    
    /*
    * Counters for creating new way ids and for creating new problem ids
    */
    private Long minWayId;
    private Long maxProblemId;
    
    /*
    * Maps used to save OSM data in format acceptable by Solver.
    */
    private final Map<Long,Station> stations;
    private final Map<Station,Long> stationNodeMap;
    private Map<Long, Station> replaceStationMap;
    private final Map<Long,List<Long>> relWayMap;
    private final Map<Long,List<Long>> wayNodeMap;
    private final Map<String,List<Long>> lineRelMap;
    
    /*
    * Maps used to save new/updated OSM data from {@code SolutionHandler}
    */
    private Map<MLCMP, Ordering> ordering;
    
    private final HashMap<Track,List<Long>> trackWayMap;
    private final HashMap<Long,List<Track>> wayTrackMap;
    
    /*
    * CREATE
    */
    private final HashMap<Long,PlanetOsmRels> rels_C;
    private final HashMap<Long,PlanetOsmWays> ways_C;
    private final HashMap<Long,PlanetOsmNodes> nodes_C;
    
    private final HashMap<Long,PlanetOsmRels> rels_print_C;
    private final HashMap<Long,PlanetOsmWays> ways_print_C;
    private final HashMap<Long,PlanetOsmNodes> nodes_print_C;
    
    /*
    * MODIFY
    */
    private final HashMap<Long,PlanetOsmRels> rels_M;
    private final HashMap<Long,PlanetOsmWays> ways_M;
    private final HashMap<Long,PlanetOsmRels> post_solution_rels_M;
    private final HashMap<Long,PlanetOsmWays> post_solution_ways_M;
    private final HashMap<Long,PlanetOsmNodes> nodes_M;
    
    private final HashMap<Long,PlanetOsmRels> rels_print_M;
    private final HashMap<Long,PlanetOsmWays> ways_print_M;
    private final HashMap<Long,PlanetOsmNodes> nodes_print_M;
    
    /*
    * DELETE
    */
    private final HashMap<Long,PlanetOsmRels> rels_D;
    private final HashMap<Long,PlanetOsmWays> ways_D;
    private final HashMap<Long,PlanetOsmNodes> nodes_D;
    
    private final HashMap<Long,PlanetOsmRels> rels_print_D;
    private final HashMap<Long,PlanetOsmWays> ways_print_D;
    private final HashMap<Long,PlanetOsmNodes> nodes_print_D;
    
    private DataStore(){
        relations = new HashMap<>();
        otherRelations = new HashMap<>();
        fetchedRelations = new HashMap<>();
        ways = new HashMap<>();
        fetchedWays = new HashMap<>();
        nodes = new HashMap<>();
        fetchedNodes = new HashMap<>();
        
        problematicWays = new HashSet<>();
        
        relWayMap = new HashMap<>();
        wayNodeMap = new HashMap<>();
        stations = new HashMap<>();
        stationNodeMap = new HashMap<>();
        lineRelMap = new HashMap<>();
        trackWayMap = new HashMap<>();
        wayTrackMap = new HashMap<>();
        oldRels = new HashSet<>();
        oldProblems = new ArrayList<>();
        newProblems = new ArrayList<>();
        splitWays = new ArrayList<>();
        wayMap = new HashMap<>();
        oldWayMap = new HashMap<>();
        reverseWayMap = new HashMap<>();
        
        rels_C = new HashMap<>();
        ways_C = new HashMap<>();
        nodes_C = new HashMap<>();
        rels_print_C = new HashMap<>();
        ways_print_C = new HashMap<>();
        nodes_print_C = new HashMap<>();
        
        rels_M = new HashMap<>();
        ways_M = new HashMap<>();
        nodes_M = new HashMap<>();
        rels_print_M = new HashMap<>();
        ways_print_M = new HashMap<>();
        nodes_print_M = new HashMap<>();
        post_solution_rels_M = new HashMap<>();
        post_solution_ways_M = new HashMap<>();
        
        rels_D = new HashMap<>();
        ways_D = new HashMap<>();
        nodes_D = new HashMap<>();
        rels_print_D = new HashMap<>();
        ways_print_D = new HashMap<>();
        nodes_print_D = new HashMap<>();
    }
    
    public static DataStore getInstance(){
        if(instance == null)instance = new DataStore();
        return instance;
    }

    public HashMap<Long, WayMap> getWayMap() {
        return wayMap;
    }

    public HashMap<Long, WayMap> getOldWayMap() {
        return oldWayMap;
    }
    
    public ArrayList<Long> getSplitWays() {
        return splitWays;
    }
    
    public HashMap<Long, PlanetOsmRels> getFetchedRelations() {
        return fetchedRelations;
    }

    public HashMap<Long, PlanetOsmWays> getFetchedWays() {
        return fetchedWays;
    }

    public HashMap<Long, PlanetOsmNodes> getFetchedNodes() {
        return fetchedNodes;
    }

    public HashMap<Long, PlanetOsmRels> getPost_solution_rels_M() {
        return post_solution_rels_M;
    }

    public HashMap<Long, PlanetOsmWays> getPost_solution_ways_M() {
        return post_solution_ways_M;
    }
    
    public HashMap<Long, PlanetOsmRels> getOtherRelations() {
        return otherRelations;
    }
    
    public HashMap<Long, Long> getReverseWayMap() {
        return reverseWayMap;
    }
    
    public HashMap<Long, PlanetOsmRels> getRelations() {
        return relations;
    }

    public HashMap<Long, PlanetOsmWays> getWays() {
        return ways;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes() {
        return nodes;
    }

    public Map<Long, Station> getStations() {
        return stations;
    }

    public Map<Station, Long> getStationNodeMap() {
        return stationNodeMap;
    }

    public Map<Long, Station> getReplaceStationMap() {
        return replaceStationMap;
    }

    public Map<Long, List<Long>> getRelWayMap() {
        return relWayMap;
    }

    public Map<Long, List<Long>> getWayNodeMap() {
        return wayNodeMap;
    }

    public Map<String, List<Long>> getLineRelMap() {
        return lineRelMap;
    }

    public Map<MLCMP, Ordering> getOrdering() {
        return ordering;
    }

    public void setOrdering(Map<MLCMP, Ordering> ordering) {
        this.ordering = ordering;
    }

    public HashMap<Track, List<Long>> getTrackWayMap() {
        return trackWayMap;
    }

    public HashMap<Long, List<Track>> getWayTrackMap() {
        return wayTrackMap;
    }

    public HashMap<Long, PlanetOsmRels> getRels_print_C() {
        return rels_print_C;
    }

    public HashMap<Long, PlanetOsmWays> getWays_print_C() {
        return ways_print_C;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_print_C() {
        return nodes_print_C;
    }

    public HashMap<Long, PlanetOsmRels> getRels_print_M() {
        return rels_print_M;
    }

    public HashMap<Long, PlanetOsmWays> getWays_print_M() {
        return ways_print_M;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_print_M() {
        return nodes_print_M;
    }

    public HashMap<Long, PlanetOsmRels> getRels_print_D() {
        return rels_print_D;
    }

    public HashMap<Long, PlanetOsmWays> getWays_print_D() {
        return ways_print_D;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_print_D() {
        return nodes_print_D;
    }

    public HashSet<Long> getOldRels() {
        return oldRels;
    }
    
    public HashMap<Long, PlanetOsmRels> getRels_C() {
        return rels_C;
    }

    public HashMap<Long, PlanetOsmWays> getWays_C() {
        return ways_C;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_C() {
        return nodes_C;
    }

    public HashMap<Long, PlanetOsmRels> getRels_M() {
        return rels_M;
    }

    public HashMap<Long, PlanetOsmWays> getWays_M() {
        return ways_M;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_M() {
        return nodes_M;
    }

    public HashMap<Long, PlanetOsmRels> getRels_D() {
        return rels_D;
    }

    public HashMap<Long, PlanetOsmWays> getWays_D() {
        return ways_D;
    }

    public HashMap<Long, PlanetOsmNodes> getNodes_D() {
        return nodes_D;
    }

    public ArrayList<GraphComponents> getOldProblems() {
        return oldProblems;
    }
    
    public ArrayList<GraphComponents> getNewProblems() {
        return newProblems;
    }

    public HashSet<Long> getProblematicWays() {
        return problematicWays;
    }

    public Long getMinWayId() {
        return minWayId;
    }

    public void setMinWayId(Long minWayId) {
        this.minWayId = minWayId-1;
    }

    public void decreaseMinWayId(){
        this.minWayId--;
    }
    
    public Long getMaxProblemId() {
        return maxProblemId;
    }

    public void setMaxProblemId(Long maxProblemId) {
        this.maxProblemId = maxProblemId+1;
    }

    public void increaseMaxProblemId(){
        this.maxProblemId++;
    }
}
