/*
 * Copyright (C) 2014 Intelligent Data Analysis research lab,
 Czech Technical University in Prague
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.felk.ida.monocle.osm.xml;

import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.logs.LogWriter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Pavel Vňuk
 */
public class XMLWriter {
    private final DataStore ds = DataStore.getInstance();
    private final File output;
    
    public XMLWriter(File output){
        this.output = output;
    }
    
    public void writeXML(){
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(output));
            
            bw.write("<osmChange>");
            bw.newLine();
            //nodes to create
            bw.write("<create>");
            bw.newLine();
            for (PlanetOsmNodes node : ds.getNodes_print_C().values()) {
                bw.write(node.toXML().toString());
            }
            bw.write("</create>");
            bw.newLine();
            //nodes to modify
            bw.write("<modify>");
            bw.newLine();
            for (PlanetOsmNodes node : ds.getNodes_print_M().values()) {
                bw.write(node.toXML().toString());
            }
            bw.write("</modify>");
            bw.newLine();
            //nodes to delete
            bw.write("<delete>");
            bw.newLine();
            for (PlanetOsmNodes node : ds.getNodes_print_D().values()) {
                bw.write(node.toXML().toString());
            }
            bw.write("</delete>");
            bw.newLine();
            
            //ways to create
            bw.write("<create>");
            bw.newLine();
            for (PlanetOsmWays way : ds.getWays_print_C().values()) {
                bw.write(way.toXML().toString());
            }
            bw.write("</create>");
            bw.newLine();
            //ways to modify
            bw.write("<modify>");
            bw.newLine();
            for (PlanetOsmWays way : ds.getWays_print_M().values()) {
                bw.write(way.toXML().toString());
            }
            bw.write("</modify>");
            bw.newLine();
            //ways to delete
            bw.write("<delete>");
            bw.newLine();
            for (PlanetOsmWays way : ds.getWays_print_D().values()) {
                bw.write(way.toXML().toString());
            }
            bw.write("</delete>");
            bw.newLine();
            
            //rels to create
            bw.write("<create>");
            bw.newLine();
            for (PlanetOsmRels rel : ds.getRels_print_C().values()) {
                bw.write(rel.toXML().toString());
            }
            bw.write("</create>");
            bw.newLine();
            //rels to modify
            bw.write("<modify>");
            bw.newLine();
            for (PlanetOsmRels rel : ds.getRels_print_M().values()) {
                bw.write(rel.toXML().toString());
            }
            bw.write("</modify>");
            bw.newLine();
            //rels to delete
            bw.write("<delete>");
            bw.newLine();
            for (PlanetOsmRels rel : ds.getRels_print_D().values()) {
                bw.write(rel.toXML().toString());
            }
            bw.write("</delete>");
            bw.newLine();
            
            bw.write("</osmChange>");
        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: Cannot write to xml output file.");
        }
    }
}
