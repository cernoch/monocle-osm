package cz.cvut.felk.ida.monocle.osm;

import cz.cvut.fel.rysavpe1.mlcmp.model.MLCMP;
import cz.cvut.fel.rysavpe1.mlcmp.ordermodel.Ordering;
import cz.cvut.fel.rysavpe1.mlcmp.output.PGFSolutionWriter;
import cz.cvut.felk.ida.monocle.osm.configs.ConfigLoader;
import cz.cvut.felk.ida.monocle.osm.db.DBHandler;
import cz.cvut.felk.ida.monocle.osm.solverCom.ContractionListener;
import cz.cvut.felk.ida.monocle.osm.data.DataStore;
import cz.cvut.felk.ida.monocle.osm.data.DifferenceChecker;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmNodes;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmRels;
import cz.cvut.felk.ida.monocle.osm.db.PlanetOsmWays;
import cz.cvut.felk.ida.monocle.osm.logs.LogWriter;
import cz.cvut.felk.ida.monocle.osm.solverCom.ListenedRun;
import cz.cvut.felk.ida.monocle.osm.solverCom.SolutionHandler;
import cz.cvut.felk.ida.monocle.osm.solverCom.SolverLoader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;



public class Application {
    /*
    * Main priority commands.
    */
    private static final String FIRST_RUN = "-fr";
    private static final String DIFF_RUN = "-dr";
    private static final String HELP = "-help";
    
    private static int mode;
    
    private static final DataStore DS = DataStore.getInstance();
    
    public static void main(String[] args) {
        
        ConfigLoader cl = new ConfigLoader();
        LogWriter.setIssueLog(new File(cl.getIssueLogFile()));
        LogWriter.setLog(new File(cl.getLogFile()));
        
        if(args.length == 0){
            LogWriter.writeIssue("Critical: Application will not run without arguments.");
            System.exit(0);
        }
        
        switch(args[0]){
            case FIRST_RUN:mode=1;break;
            case DIFF_RUN:mode=2;break;
            case HELP:printHelp();break;
            default: LogWriter.writeIssue("Critical: Invalid argument, application will not proceed.");System.exit(0);
        }
        
        DataStore ds = DataStore.getInstance();
        DBHandler dh = DBHandler.getInstance();
        DifferenceChecker dc = new DifferenceChecker();
        
        //SOLVER ONLY SETTINGS
        MLCMP bigProblem;
        int limit = 600000;
        int n = 10;
        int vlb = 0;
        boolean split = true;
        
        switch(mode){
            case 1:
                dh.fetchDBDataFT();
                break;
            case 2:
                dc.check();
                break;
            default: LogWriter.writeIssue("Critical: Inside default option - solving part.");
        }
        
        try{
            SolverLoader sl = new SolverLoader(cl.getColors(),mode);
            sl.loadSolver();
            bigProblem  = sl.getProblem();

            Map<MLCMP, Ordering> map = ListenedRun.run(bigProblem, new ContractionListener(),limit, split);
            ds.setOrdering(map);
            SolutionHandler sh = new SolutionHandler(mode);
            sh.backtracing();
            sh.checkSplitWays();
            sh.preparePrintMaps();

        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: I/O exception during solving.");
        } catch (InterruptedException ex) {
            LogWriter.writeIssue("Critical: Interrupted during solving.");
        }
        
        printDiff();
        dh.saveWayMaps(mode);
        dh.saveGraphComponents(mode);
        dh.closeGraphFactory();
        dh.closeOSMFactory();
    }
    
    
    
    private static void printHelp(){
        System.out.println("Help for ...");
        System.out.println("Application accepts 3 main commands as first argument:");
        System.out.println("-fr  for first run - all OSM data are taken from database");
        System.out.println("-dr  for diff run - application checks diff for changes in OSM data");
        System.out.println("-help  for help");
        System.out.println("Application accepts 1 secondary commands as second argument:");
        System.out.println("-conf  for run, with configuration files");
        System.out.println("if -conf command is accepted application can read path as third argument - optional");
        System.out.println("any other command after -conf will be ignored");
        System.out.println("Application accepts 2 tertiary commands as other arguments:");
        System.out.println("-diff  for alteration of path to diff file, another argument is always taken as path");
        System.out.println("-out  for alteration of path to where output should be placed, another argument is always taken as path");
    }
    
    @SuppressWarnings("UseOfSystemOutOrSystemErr")
    private static void throwError(String error){
        System.err.println(error);
        System.err.println("For usage options rerun with parameter -help.");
        System.exit(1);
    }
    
    private static Path checkFile(Path path){
        if (!Files.exists(path)){
            try{
                Files.createFile(path);
            }catch (IOException ex){
                System.err.println("Cannot write the output file. I will use system output instead.");
                ex.printStackTrace();
                return null;
            }
        }
        if (!Files.isWritable(path)){
            System.err.println("Cannot write the output file. I will use system output instead.");
            return null;
        }
        return path;
    }
    
    private static void printDiff(){
        File output = new File("output.txt");
        BufferedWriter bwl;
        try {
            bwl = new BufferedWriter(new FileWriter(output));
            if(!DS.getNodes_print_C().isEmpty()){
                bwl.write("<create>\n");
                for (PlanetOsmNodes node : DS.getNodes_print_C().values()) {
                    bwl.write(node.toXML().toString());
                }
                bwl.write("</create>\n");
            }
            if(!DS.getNodes_print_M().isEmpty()){
                bwl.write("<modify>\n");
                for (PlanetOsmNodes node : DS.getNodes_print_M().values()) {
                    bwl.write(node.toXML().toString());
                }
                bwl.write("</modify>\n");
            }
            if(!DS.getNodes_print_D().isEmpty()){
                bwl.write("<delete>\n");
                for (PlanetOsmNodes node : DS.getNodes_print_D().values()) {
                    bwl.write(node.toXML().toString());
                }
                bwl.write("</delete>\n");
            }
            if(!DS.getWays_print_C().isEmpty()){
                bwl.write("<create>\n");
                for (PlanetOsmWays way : DS.getWays_print_C().values()) {
                    bwl.write(way.toXML().toString());
                }
                bwl.write("</create>\n");
            }
            if(!DS.getWays_print_M().isEmpty()){
                bwl.write("<modify>\n");
                for (PlanetOsmWays way : DS.getWays_print_M().values()) {
                    bwl.write(way.toXML().toString());
                }
                bwl.write("</modify>\n");
            }
            if(!DS.getWays_print_D().isEmpty()){
                bwl.write("<delete>\n");
                for (PlanetOsmWays way : DS.getWays_print_D().values()) {
                    bwl.write(way.toXML().toString());
                }
                bwl.write("</delete>\n");
            }
            if(!DS.getRels_print_C().isEmpty()){
                bwl.write("<create>\n");
                for (PlanetOsmRels rel : DS.getRels_print_C().values()) {
                    bwl.write(rel.toXML().toString());
                }
                bwl.write("</create>\n");
            }
            if(!DS.getRels_print_M().isEmpty()){
                bwl.write("<modify>\n");
                for (PlanetOsmRels rel : DS.getRels_print_M().values()) {
                    bwl.write(rel.toXML().toString());
                }
                bwl.write("</modify>\n");
            }
            if(!DS.getRels_print_D().isEmpty()){
                bwl.write("<delete>\n");
                for (PlanetOsmRels rel : DS.getRels_print_D().values()) {
                    bwl.write(rel.toXML().toString());
                }
                bwl.write("</delete>\n");
            }
        } catch (IOException ex) {
            LogWriter.writeIssue("Critical: Cannot write to diff output file.");
        }
    }
    
    private static void printToTex(Path input, Map<MLCMP, Ordering> map) throws IOException{
        String fileName2 = input.getFileName().toString();
        fileName2 = fileName2.substring(0, fileName2.indexOf('.'));
        final Path dir2 = input.toAbsolutePath().getParent();
        @SuppressWarnings("UseOfSystemOutOrSystemErr")
        PGFSolutionWriter out = new PGFSolutionWriter(new OutputStreamWriter(System.err));
        int i = 0;
        for (Map.Entry<MLCMP, Ordering> e : map.entrySet()){
            String end = map.size() == 1 ? ".tex" : i++ + ".tex";
            final Path outfile = checkFile(dir2.resolve(fileName2 + end));
            if (outfile == null)throwError("Cannot write the output file.");
            out.writeGraph(outfile, e.getKey(), e.getValue());
            try{
                    final String command = "pdflatex \"" + outfile.toAbsolutePath().toString() + "\" -interaction=nonstopmode -no-file-line-error";
                    Runtime.getRuntime().exec(command);
            }catch (Exception ex){
                    System.err.println("Cannot write the map to pdf. Try to compile alone using pdflatex.");
            }
        }
    }
}
