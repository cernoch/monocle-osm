<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  
  <groupId>cz.cvut.felk.ida.monocle</groupId>
  <artifactId>osm</artifactId>
  <version>0.1-SNAPSHOT</version>

  <name>Monocle OSM</name>
  <description>Integration of Metro Map Line Crossing Minimization
      solver into the OpenStreetMap tile server.</description>

  <licenses>
    <license>
      <name>GNU General Public License (GPL) version 3.0</name>
      <url>http://www.gnu.org/licenses/gpl-3.0.html</url>
    </license>
  </licenses>
  
  <organization>
    <name>Intelligent Data Analysis research lab,
      Czech Technical University in Prague</name>
    <url>http://ida.felk.cvut.cz</url>
  </organization>

  <developers>
    <developer>
      <id>pavel</id>
      <name>Pavel Vňuk</name>
      <email>pav.vnuk@gmail.com</email>
    </developer>
    <developer>
      <id>radek</id>
      <name>Radomír Černoch</name>
      <email>radomir.cernoch@gmail.com</email>
    </developer>
  </developers>

  <properties>
    <mainClass>cz.cvut.felk.ida.monocle.osm.Application</mainClass>
    <netbeans.hint.license>GNUGPL3</netbeans.hint.license>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <scm>
    <url>https://bitbucket.org/cernoch/monocle</url>
    <connection>scm:git:https://bitbucket.org/cernoch/monocle-osm.git</connection>
    <developerConnection>scm:git:git@bitbucket.org:cernoch/monocle-osm.git</developerConnection>
    <tag>HEAD</tag>
  </scm>
  
  <repositories>
    <repository><id>ida</id><!-- IDA CVUT repository -->
        <url>https://ida.felk.cvut.cz/maven2/</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>sonatype-nexus-staging</id>
      <name>Sonatype Nexus release repository</name>
      <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
    </repository>
    <snapshotRepository>
      <id>sonatype-nexus-snapshots</id>
      <name>Sonatype Nexus snapshot repository</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>

  <dependencies>
    <dependency><!-- Monocle solver -->
      <groupId>cz.cvut.felk.ida.monocle</groupId>
      <artifactId>api</artifactId>
      <version>1.1</version>
    </dependency>
    <!-- Test -->
    <dependency><!-- JUnit -->
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.eclipse.persistence</groupId>
      <artifactId>eclipselink</artifactId>
      <version>2.6.0-M3</version>
    </dependency>
    <dependency>
      <groupId>postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <version>9.1-901.jdbc4</version>
    </dependency>
    <dependency>
      <groupId>cz.cvut.felk.ida.monocle</groupId>
      <artifactId>input</artifactId>
      <version>1.0</version>
      <type>jar</type>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.3.2</version>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin><!-- JAR assembler -->
        <artifactId>maven-assembly-plugin</artifactId>
        <version>2.4</version>
        <configuration>
          <descriptors>
            <descriptor>src/main/assembly/standalone.xml</descriptor>
          </descriptors>
          <archive><manifest>
            <mainClass>${mainClass}</mainClass>
            </manifest></archive>
        </configuration>
        <executions><execution>
          <phase>package</phase>
          <goals><goal>single</goal></goals>
          </execution></executions>
      </plugin>
      <plugin><!-- Compile using Java 1.7 -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>2.5.1</version>
        <configuration>
          <source>1.7</source>
          <target>1.7</target>
          <encoding>${project.build.sourceEncoding}</encoding>
        </configuration>
      </plugin>
      <plugin><!-- Use UTF-8 in Netbeans even on Windows -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-resources-plugin</artifactId>
        <version>2.4.3</version>
        <configuration>
          <encoding>${project.build.sourceEncoding}</encoding>
        </configuration>
      </plugin>
      <plugin><!-- Test executor -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.15</version>
      </plugin>      
      <plugin><!-- Release tool -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-release-plugin</artifactId>
        <version>2.4</version>
        <configuration>
          <tagNameFormat>v@{project.version}</tagNameFormat>
          <scmCommentPrefix>Release:</scmCommentPrefix>
        </configuration>
      </plugin>
      <plugin><!-- SCM plugin for GIT -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-scm-plugin</artifactId>
        <version>1.8.1</version>
        <configuration>
          <connectionType>developerConnection</connectionType>
        </configuration>
      </plugin>
      <plugin><!-- GPG plugin for signing -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>1.4</version>
        <executions>
          <execution>
            <id>sign-artifacts</id>
            <phase>verify</phase>
            <goals><goal>sign</goal></goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
    <extensions>
      <extension><!-- SCP connector -->
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-ssh</artifactId>
        <version>1.0</version>
      </extension>
    </extensions>
  </build>
</project>

